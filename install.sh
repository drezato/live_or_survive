touch install.sh
nautilus .

sudo mkdir /mnt/code
sudo chmod 777 -R /mnt/code

HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
copy to:
https://gitlab.com/-/user_settings/ssh_keys

cd /mnt/code
git clone git@gitlab.com:drezato/live_or_survive.git

cd live_or_survive/
git config --global user.email "kokito@gmail.com"
git config --global user.name Kokito
sudo snap install pycharm-community --classic

sudo apt install python3.8 -y
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
sudo apt install python3-pip
pip install pipenv==2023.3.20
sudo pip install pipenv==2023.3.20
echo "export PATH=$PATH:~/.local/bin/" >> ~/.bashrc
source ~/.bashrc
pipenv install --dev
pipenv shell

/snap/pycharm-community/current/bin/pycharm.sh . 2>&1 > /dev/null &

uvicorn backend.main:app --reload